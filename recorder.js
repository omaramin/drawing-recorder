'use strict';

// Nodes to be used in the LinkedList class
class Node {
    constructor(val = 0) {
        this.val = val;
        this.next = null;
    }
}

// LinkedList class
class LinkedList {
    constructor(start = null) {
        this.start = start;
    }

    addFront(newStart) {
        if (this.size() == 0) {
            this.start = newStart;
        }
        else {
            newStart.next = this.start;
            this.start = newStart;
        }
    }

    addMid(newMid) {
        if (this.size() == 0) {
            this.start = newMid;
        }
        else {
            newMid.next = this.start.next;
            this.start.next = newMid;
        }
    }

    addEnd(newEnd) {
        if (this.size() == 0) {
            this.start = newEnd;
        }
        else {
            let curr = this.start;
            while (curr.next != null) {
                curr = curr.next;
            }
            curr.next = newEnd;
        }
    }

    find(key) {
        let curr = this.start;
        while (curr != null) {
            if (curr.val == key) {
                return curr;
            }
            curr = curr.next;
        }
        return -1;
    }
  
    size(curr = this.start) {
        if (this.start == null) {
            return 0;
        }
        else {
        	if (curr == null) {
            	return 0;
            }
            else {
            	return 1 + this.size(curr.next);
            }
        }
    }
}

class Recorder {
    constructor(path = null, lines = 0) {
        this.path = path;
        this.lines = lines;
    }

    startRec() {
        recording = true;
        this.path = [];
        this.lines = 0;
    }

    stopRec() {
        recording = false;
    }

    play(record = rec.path) {
        if (confirm("Are you sure you want to play your recording? This will clear the current canvas.")) {
            // Empty the board
            this.clear();
            // Loop through entire path
            for (let y = 0; y < this.path.length; y++) {
                let curr = this.path[y].start.next.next;
                let prev = this.path[y].start.next.val;

                let i = 0;

                // Delay between strokes
                setTimeout(function() {
                    // Loop through each line stroke
                    while (i < record[y].size() - 3) {
                        // Delay between pixels of a stroke
                        setTimeout(function() {
                            cont.beginPath();
                            
                            // Styling
                            cont.lineWidth = 3;
                            cont.lineCap = 'round';
                            
                            // Starting point
                            cont.moveTo(prev.x, prev.y);
                            
                            // Ending point and drawing
                            cont.lineTo(curr.val.x , curr.val.y);
                            cont.stroke();
                            
                            // Move to next pixel
                            prev = {x: curr.val.x, y: curr.val.y}
                            curr = curr.next;
                        }, 3 * i);
                        i++;
                    }
                }, 800 * y);
            }
        }
        else { 
            // Do nothing?
        }
    }

    saveRec() {
        window.localStorage.setItem('rec', JSON.stringify(rec));
    }

    loadRec() {
        let loaded = JSON.parse(window.localStorage.getItem('rec'));
        let loadRec = new Recorder([], loaded.lines);

        // Fix for JSON parsing not recognizing custom classes
        for (let h = 0; h < loaded.lines; h++) {
            // New stroke
            let lst = new LinkedList();

            // Load starting point of stroke
            let curr = loaded.path[h].start;
            // Add pixels from loaded
            while (curr != null) {
                let st = new Node({x:curr.val.x, y:curr.val.y})
                lst.addEnd(st);
                curr = curr.next;
            }
            // Add stroke to full path
            loadRec.path.push(lst);
        }
        // Play loaded drawing
        loadRec.play(loadRec.path);
    }

    setColor() {
        let newColor = document.getElementById("color").value;
        cont.strokeStyle = newColor;
    }

    clear() {
        cont.clearRect(0, 0, canvas.width, canvas.height);
    }
}

// Drawing
let canvas = document.getElementById("canvas");
let cont = canvas.getContext("2d");
let drawing = false;
let recording = false;
let pos = {x:0, y:0};

// Create recorder
let rec = new Recorder([], 0);

// Set event listeners
canvas.addEventListener('mousedown', clicked);
canvas.addEventListener('mousemove', draw);
canvas.addEventListener('mouseup', released);

// Check if lmb pressed
function clicked(e) {
    drawing = true;
    let st = new Node({x:pos.x, y:pos.y});
    let lst = new LinkedList(st);

    // Add new stroke to full path
    rec.path.push(lst);
    currPos(e);
}

// Check if lmb released
function released() {
    drawing = false;

    // Count end of stroke
    rec.lines += 1;
}

// Update position of cursor and record
function currPos(e) { 
    pos.x = e.clientX - canvas.offsetLeft;
    pos.y = e.clientY - canvas.offsetTop;

    // Record if flag is set
    if (recording == true) {
        let end = new Node({x:pos.x, y:pos.y});
        rec.path[rec.lines].addEnd(end);
    }
}

// Draw the path
function draw(e) {
    if (drawing == true) {
        cont.beginPath();
        
        // Styling
        cont.lineWidth = 3;
        cont.lineCap = 'round';
        
        // Starting point
        cont.moveTo(pos.x, pos.y);
        currPos(e);
        
        // Ending point and draw
        cont.lineTo(pos.x , pos.y);
        cont.stroke();
    }
    return;
}